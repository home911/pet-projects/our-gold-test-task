# our_gold

Тестовое задание от компании "Наше золото"

# Запуск

## Flutter (3.13.9)

```flutter run```

### Автоматический запуск build_runner (для генерации riverpod)

`dart run build_runner watch`

### YandexMap

Добавить ключ от YandexMap MobileSDK в файлы [AppDelegate.swift](ios/Runner/AppDelegate.swift)
и [MainActivity.kt](android/app/src/main/kotlin/ru/pav/our_gold/MainActivity.kt)