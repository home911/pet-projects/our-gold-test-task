/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:our_gold/core/constants/size.dart';
import 'package:our_gold/main.dart';

final themeModeProvider = StateProvider<ThemeMode>((ref) => ThemeMode.light);

final themeProvider = Provider<ThemeData>(
  (ref) => _theme.copyWith(
    iconTheme: IconThemeData(color: _theme.primaryColor),
    listTileTheme: ListTileThemeData(iconColor: _theme.primaryColor),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
          backgroundColor: _theme.primaryColor,
          foregroundColor: _theme.colorScheme.onPrimary,
          textStyle: _theme.textTheme.titleLarge?.copyWith(fontWeight: FontWeight.bold),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(kSRadius))),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
        side: BorderSide(color: _theme.primaryColor),
        foregroundColor: _theme.colorScheme.onBackground,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(kSRadius)),
      ),
    ),
    inputDecorationTheme: _theme.inputDecorationTheme.copyWith(
      contentPadding: const EdgeInsets.symmetric(vertical: kSPadding, horizontal: kLPadding),
      enabledBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.black)),
    ),
  ),
);

final _theme = FlexThemeData.light(
  colors: FlexSchemeColor.from(primary: const Color(0xffba9021)),
  subThemesData: const FlexSubThemesData(
    navigationBarHeight: 60,
    inputDecoratorIsFilled: false,
    adaptiveAppBarScrollUnderOff: FlexAdaptive.all(),
    adaptiveRemoveElevationTint: FlexAdaptive.all(),
    blendOnColors: false,
  ),
  visualDensity: FlexColorScheme.comfortablePlatformDensity,
  useMaterial3: true,
  swapLegacyOnMaterial3: true,
  fontFamily: GoogleFonts.roboto().fontFamily,
);

final _customThemeProvider = Provider<CustomThemeData>((ref) {
  return const CustomThemeData(
    success: Color(0xff149702),
  );
});

extension CustomTheme on ThemeData {
  CustomThemeData get custom => container.read(_customThemeProvider);
}

class CustomThemeData {
  final Color success;

  const CustomThemeData({
    required this.success,
  });
}
