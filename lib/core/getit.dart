/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:our_gold/core/utils/logger.dart';
import 'package:our_gold/core/utils/message_util.dart';
import 'package:our_gold/core/utils/storage/storage_util.dart';
import 'package:our_gold/core/utils/storage/storage_util_secure.dart';
import 'package:our_gold/features/login/data/auth_repository_impl.dart';
import 'package:our_gold/features/login/domain/repository/auth_repository.dart';
import 'package:our_gold/features/main/data/user_repository_impl.dart';
import 'package:our_gold/features/main/domain/repository/user_repository.dart';
import 'package:our_gold/features/scanner/data/product_repository_impl.dart';
import 'package:our_gold/features/scanner/domain/repository/product_repository.dart';
import 'package:our_gold/features/stores/data/store_repository_impl.dart';
import 'package:our_gold/features/stores/domain/repository/store_repository.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';


final getIt = GetIt.I;

Future<void> setupGetIt() async {
  getIt.registerLazySingleton<MessageUtil>(() => MessageUtilSnackBar());
  getIt.registerLazySingleton<ProductRepository>(() => ProductRepositoryImpl());
  getIt.registerLazySingleton<AuthRepository>(() => AuthRepositoryImpl());
  getIt.registerLazySingleton<UserRepository>(() => UserRepositoryImpl());
  getIt.registerLazySingleton<StoreRepository>(() => StoreRepositoryImpl());
  getIt.registerLazySingleton<StorageUtil>(() => StorageUtilSecure());
  getIt.registerLazySingleton<Dio>(() => Dio()..interceptors.add(PrettyDioLogger()));
  getIt.registerLazySingleton<Log>(() => Log());
}
