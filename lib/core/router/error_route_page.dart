/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'package:our_gold/core/router/router_provider.dart';
import 'package:our_gold/features/login/presentation/page/login_page.dart';

class ErrorRoutePage extends StatelessWidget {
  const ErrorRoutePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Страница не найдена'),),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Страницы по указанному пути найдено не было', style: Theme.of(context).textTheme.bodyMedium),
            TextButton(onPressed: () => router.goNamed(LoginPage.name), child: const Text('Перейти на домашнюю страницу'))
          ],
        ),
      ),
    );
  }
}
