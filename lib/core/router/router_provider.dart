/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

// ignore_for_file: unnecessary_string_interpolations

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:our_gold/features/login/presentation/page/login_page.dart';
import 'package:our_gold/features/login/presentation/provider/auth_provider.dart';
import 'package:our_gold/features/main/presentation/components/main_bottom_bar.dart';
import 'package:our_gold/features/main/presentation/page/main_page.dart';
import 'package:our_gold/features/scanner/presentation/page/scanner_page.dart';
import 'package:our_gold/features/stores/presentation/page/stores_page.dart';
import 'package:our_gold/main.dart';

import 'error_route_page.dart';

final GlobalKey<NavigatorState> _rootNavigatorKey = GlobalKey<NavigatorState>(debugLabel: 'root');

final GoRouter router = GoRouter(
  navigatorKey: _rootNavigatorKey,
  initialLocation: '/${LoginPage.name}',
  errorBuilder: (context, state) => const ErrorRoutePage(),
  routes: [
    StatefulShellRoute.indexedStack(
      builder: (context, state, shell) => MainBottomBar(navigationShell: shell),
      branches: [
        StatefulShellBranch(
          routes: [
            GoRoute(
              name: MainPage.name,
              path: '/${MainPage.name}',
              redirect: _redirectToLogin,
              builder: (context, state) => const MainPage(),
            ),
          ],
        ),
        StatefulShellBranch(
          routes: [
            GoRoute(
              name: ScannerPage.name,
              path: '/${ScannerPage.name}',
              redirect: _redirectToLogin,
              builder: (context, state) => ScannerPage(),
            )

          ],
        ),
        StatefulShellBranch(
          routes: [
            GoRoute(
              name: StoresPage.name,
              path: '/${StoresPage.name}',
              redirect: _redirectToLogin,
              builder: (context, state) => const StoresPage(),
            ),
          ],
        ),
      ],
    ),
    GoRoute(
      name: LoginPage.name,
      path: '/${LoginPage.name}',
      redirect: (context, state) async => await container.read(checkTokenProvider.future) ? '/${MainPage.name}' : null,
      builder: (context, state) => const LoginPage(),
    ),
  ],
  debugLogDiagnostics: true,
);

Future<String?> _redirectToLogin(BuildContext context, GoRouterState state) async =>
    await container.read(checkTokenProvider.future) ? null : '/${LoginPage.name}';

CustomTransitionPage<Object?> transition({required Widget child}) {
  return CustomTransitionPage(
    child: child,
    transitionsBuilder:
        (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
      return SlideTransition(
          position: animation.drive(
            Tween<Offset>(
              begin: const Offset(0.75, 0),
              end: Offset.zero,
            ),
          ),
          child: child);
    },
  );
}
