/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

const kXSIconSize = 17.0;
const kSIconSize = 20.0;
const kMIconSize = 25.0;
const kLIconSize = 30.0;

const kSFontSize = 18.0;
const kMFontSize = 18.0;

const kSElevation = 5.0;
const kMElevation = 10.0;
const kLElevation = 15.0;

const kSRadius = 5.0;
const kMRadius = 10.0;
const kLRadius = 15.0;

const kXXSPadding = 1.0;
const kXSPadding = 3.0;
const kSPadding = 5.0;
const kMPadding = 10.0;
const kLPadding = 15.0;
const kXLPadding = 20.0;
const kXXLPadding = 25.0;
const k3XLPadding = 30.0;
const k4XLPadding = 45.0;
const k5XLPadding = 50.0;
