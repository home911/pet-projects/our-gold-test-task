/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

class AppConstants {
  static const user = 'user';
  static const authToken = 'authToken';
}