/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

class AppUrls {
  static const apiHost = 'https://helper_dev.ourgold.ru';
}