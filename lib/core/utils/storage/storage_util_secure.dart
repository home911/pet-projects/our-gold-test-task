/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:our_gold/core/constants/keys.dart';
import 'package:our_gold/core/utils/storage/storage_util.dart';
import 'package:our_gold/features/main/domain/model/user_model.dart';

class StorageUtilSecure implements StorageUtil {
  final FlutterSecureStorage storage = const FlutterSecureStorage();

  @override
  Future<void> clearSession() async {
    await storage.delete(key: AppConstants.user);
    await storage.delete(key: AppConstants.authToken);
  }

  @override
  Future<String?> getAuthToken() async {
    final token = await storage.read(key: AppConstants.authToken);
    if (token == null) {
      return null;
    }
    return 'Bearer $token';
  }

  @override
  Future<void> writeTokenInfo(String token) async => await storage.write(key: AppConstants.authToken, value: token);

  @override
  Future<UserModel?> getUser() async {
    final user = await storage.read(key: AppConstants.user);
    return user != null ? UserModel.fromMap(jsonDecode(user)) : null;
  }

  @override
  Future<void> saveUser(UserModel user) async {
    await storage.write(key: AppConstants.user, value: jsonEncode(user.toMap()));
  }

}
