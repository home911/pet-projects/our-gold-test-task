/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:our_gold/features/main/domain/model/user_model.dart';

abstract class StorageUtil {

  Future<void> clearSession();

  Future<String?> getAuthToken();

  Future<void> writeTokenInfo(String token);

  Future<UserModel?> getUser();

  Future<void> saveUser(UserModel user);

}
