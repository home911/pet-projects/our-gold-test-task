/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:our_gold/core/constants/urls.dart';

class ApiUtil {
  static Uri get loginUri => Uri.parse('${AppUrls.apiHost}/api/mobile/login');
  static Uri get userUri => Uri.parse('${AppUrls.apiHost}/api/mobile/user');
  static Uri productUri(String barcode) => Uri.parse('${AppUrls.apiHost}/api/mobile/good/$barcode');
  static Uri get storesUri => Uri.parse('${AppUrls.apiHost}/api/mobile/geo');
}