/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:intl/intl.dart';

class FormatUtil {
  static String price(double price) => NumberFormat.currency(locale: 'ru', symbol: '₽', decimalDigits: 0).format(price);
}