/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'package:our_gold/core/constants/size.dart';
import 'package:our_gold/core/theme/app_theme.dart';
import 'package:our_gold/main.dart';

abstract class MessageUtil {
  void success(dynamic message, {int duration = 2000});

  void error(dynamic message, {int duration = 2000});

  void info(dynamic message, {int duration = 3000});
}

class MessageUtilSnackBar extends MessageUtil {

  @override
  void success(dynamic message, {int duration = 2000}) {
    scaffoldMessengerKey.currentState!.clearSnackBars();
    scaffoldMessengerKey.currentState!.showSnackBar(_createSnackBar(
      message: message,
      duration: duration,
      color: container.read(themeProvider).custom.success,
    ));
  }

  @override
  void error(dynamic message, {int duration = 2000}) {
    scaffoldMessengerKey.currentState!.clearSnackBars();
    scaffoldMessengerKey.currentState!.showSnackBar(_createSnackBar(
      message: message,
      duration: duration,
      color: container.read(themeProvider).colorScheme.error,
    ));
  }

  @override
  void info(dynamic message, {int duration = 3000}) {
    scaffoldMessengerKey.currentState!.clearSnackBars();
    scaffoldMessengerKey.currentState!.showSnackBar(_createSnackBar(
      message: message,
      duration: duration,
    ));
  }

  SnackBar _createSnackBar({message, required int duration, Color? color}) {
    return SnackBar(
      content: (message is Widget)
          ? message
          : Text(message, style: container.read(themeProvider).textTheme.bodyMedium?.copyWith(color: Colors.white)),
      backgroundColor: color,
      showCloseIcon: true,
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(kMRadius)),
      margin: const EdgeInsets.all(kXLPadding),
      duration: Duration(milliseconds: duration),
    );
  }
}
