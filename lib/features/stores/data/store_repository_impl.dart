/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'dart:io';

import 'package:dio/dio.dart';
import 'package:our_gold/core/getit.dart';
import 'package:our_gold/core/utils/api_util.dart';
import 'package:our_gold/core/utils/storage/storage_util.dart';
import 'package:our_gold/features/stores/domain/model/store_by_city.dart';
import 'package:our_gold/features/stores/domain/repository/store_repository.dart';

class StoreRepositoryImpl implements StoreRepository {
  final dio = getIt<Dio>();
  final _storage = getIt<StorageUtil>();
  
  @override
  Future<List<StoreByCity>> getAllStores() async {
    final result = await dio.getUri(
        ApiUtil.storesUri,
        options: Options(headers: {HttpHeaders.authorizationHeader: await _storage.getAuthToken()})
    );
    return (result.data as List).map((e) => StoreByCity.fromMap(e)).toList();
  }
  
}