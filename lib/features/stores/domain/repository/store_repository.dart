/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:our_gold/features/stores/domain/model/store_by_city.dart';

abstract interface class StoreRepository {

  Future<List<StoreByCity>> getAllStores();

}