/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:our_gold/core/getit.dart';
import 'package:our_gold/core/utils/logger.dart';
import 'package:our_gold/features/stores/domain/model/store_by_city.dart';
import 'package:our_gold/features/stores/domain/repository/store_repository.dart';

class StoreUseCase {
  final StoreRepository _storeRepository;

  const StoreUseCase({
    required StoreRepository storeRepository,
  }) : _storeRepository = storeRepository;

  Future<List<StoreByCity>> getAllStores() async {
    try {
      return await _storeRepository.getAllStores();
    } catch (e, st) {
      getIt<Log>().e('Ошибка при получении списка магазинов', error: e, stackTrace: st);
      rethrow;
    }
  }

}