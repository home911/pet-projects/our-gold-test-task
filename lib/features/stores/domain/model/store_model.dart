/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

class StoreModel {
  final String name;
  final String address;
  final String lat;
  final String long;

  const StoreModel({
    required this.name,
    required this.address,
    required this.lat,
    required this.long,
  });

  factory StoreModel.fromMap(Map<String, dynamic> map) {
    return StoreModel(
      name: map['name'] as String,
      address: map['address'] as String,
      lat: map['lat'] as String,
      long: map['long'] as String,
    );
  }
}