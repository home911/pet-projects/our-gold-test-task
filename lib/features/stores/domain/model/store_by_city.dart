/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'store_model.dart';

class StoreByCity {
  final String name;
  final List<StoreModel> stores;

  const StoreByCity({
    required this.name,
    required this.stores,
  });

  factory StoreByCity.fromMap(Map<String, dynamic> map) {
    return StoreByCity(
      name: map['name'] as String,
      stores: (map['stores'] as List).map((e) => StoreModel.fromMap(e)).toList(),
    );
  }
}