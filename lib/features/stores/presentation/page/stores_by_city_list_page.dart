/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:our_gold/features/stores/domain/model/store_by_city.dart';
import 'package:our_gold/features/stores/domain/model/store_model.dart';
import 'package:our_gold/features/stores/presentation/page/stores_list_page.dart';
import 'package:our_gold/features/stores/presentation/provider/stores_provider.dart';

class StoresByCityListPage extends ConsumerStatefulWidget {
  const StoresByCityListPage({super.key, required this.storesByCity});

  final List<StoreByCity> storesByCity;

  @override
  ConsumerState<StoresByCityListPage> createState() => _StoresByCityListPageState();
}

class _StoresByCityListPageState extends ConsumerState<StoresByCityListPage> with AutomaticKeepAliveClientMixin {

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (ref.watch(allCitiesProvider)) {
      return Column(
        children: [
          ListTile(
            title: Row(
              children: [
                const Text('Города '),
                Text('(${widget.storesByCity.length})', style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Theme.of(context).primaryColor)),
              ],
            ),
            visualDensity: VisualDensity.compact,
          ),
          const Divider(thickness: 0.5),
          Expanded(
            child: ListView.separated(
              itemBuilder: (context, index) => ListTile(
                title: Text('${widget.storesByCity[index].name} (${widget.storesByCity[index].stores.length})'),
                trailing: const Icon(Icons.arrow_forward_ios, size: 15),
                visualDensity: VisualDensity.compact,
                dense: true,
              ),
              separatorBuilder: (context, index) => const Divider(thickness: 0.5),
              itemCount: widget.storesByCity.length,
            ),
          ),
        ],
      );
    }
    return StoresListPage(
      stores: widget.storesByCity.fold(<StoreModel>[], (prev, cur) {
        prev.addAll(cur.stores);
        return prev;
      }),
    );
  }

}
