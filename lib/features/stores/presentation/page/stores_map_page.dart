/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'package:our_gold/features/stores/domain/model/store_by_city.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class StoresMapPage extends StatefulWidget {
  const StoresMapPage({super.key, required this.storesByCity});

  final List<StoreByCity> storesByCity;

  @override
  State<StoresMapPage> createState() => _StoresMapPageState();
}

class _StoresMapPageState extends State<StoresMapPage> with AutomaticKeepAliveClientMixin {
  late final YandexMapController _mapController;
  final List<PlacemarkMapObject> points = [];

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    for (var storeByCity in widget.storesByCity) {
      for (var store in storeByCity.stores) {
        points.add(
          PlacemarkMapObject(
            mapId: MapObjectId(store.name),
            opacity: 1,
            icon: PlacemarkIcon.single(PlacemarkIconStyle(image: BitmapDescriptor.fromAssetImage('assets/icons/map_point.png'), scale: 0.2)),
            point: Point(latitude: double.parse(store.lat), longitude: double.parse(store.long)),
          ),
        );
      }
    }
  }

  @override
  void dispose() {
    _mapController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return YandexMap(
      mapObjects: points,
      onMapCreated: (controller) async {
        _mapController = controller;
        await _mapController.moveCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
              target: points.isNotEmpty
                  ? points.first.point
                  : const Point(
                      latitude: 50,
                      longitude: 20,
                    ),
              zoom: 10,
            ),
          ),
        );
      },
    );
  }
}
