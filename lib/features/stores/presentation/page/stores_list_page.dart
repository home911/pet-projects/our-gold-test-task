/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:our_gold/core/constants/size.dart';
import 'package:our_gold/features/stores/domain/model/store_model.dart';

class StoresListPage extends StatefulWidget {
  const StoresListPage({super.key, required this.stores});

  final List<StoreModel> stores;

  @override
  State<StoresListPage> createState() => _StoresListPageState();
}

class _StoresListPageState extends State<StoresListPage> {
  final _controller = TextEditingController();
  Timer? _debounce;
  List<StoreModel> storesBySearch = [];

  @override
  Widget build(BuildContext context) {
    final isSearch = _controller.text.trim().isNotEmpty;
    return Column(
      children: [
        ListTile(
          title: Row(
            children: [
              const Text('Салоны '),
              Text('(${widget.stores.length})', style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Theme.of(context).primaryColor)),
            ],
          ),
          visualDensity: VisualDensity.compact,
        ),
        const Divider(thickness: 0.5),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: kLPadding),
          child: Theme(
            data: Theme.of(context).copyWith(inputDecorationTheme: const InputDecorationTheme()),
            child: TextField(
              controller: _controller,
              decoration: const InputDecoration(prefixIcon: Icon(Icons.search), hintText: 'Поиск по адресу'),
              onChanged: (value) {
                if (_debounce?.isActive ?? false) _debounce?.cancel();
                _debounce = Timer(const Duration(seconds: 1), () => setState(() {
                  value = value.toLowerCase();
                  storesBySearch = widget.stores.where((element) => element.address.toLowerCase().contains(value)).toList();
                }));
              },
            ),
          ),
        ),
        Expanded(
          child: ListView.separated(
            itemBuilder: (context, index) {
              final store = isSearch ? storesBySearch[index] : widget.stores[index];
              return ListTile(
                title: Text(store.name, style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.bold)),
                subtitle: Text(store.address),
                visualDensity: VisualDensity.compact,
              );
            },
            separatorBuilder: (context, index) => const Divider(thickness: 0.5),
            itemCount: isSearch ? storesBySearch.length : widget.stores.length,
          ),
        ),
      ],
    );
  }
}
