/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:our_gold/core/constants/size.dart';
import 'package:our_gold/features/stores/presentation/page/stores_by_city_list_page.dart';
import 'package:our_gold/features/stores/presentation/provider/stores_provider.dart';

import 'stores_map_page.dart';

class StoresPage extends ConsumerWidget {
  static const name = 'storesPage';

  const StoresPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ref.watch(getAllStoresProvider).when(
          data: (data) => SafeArea(
            child: DefaultTabController(
              length: 2,
              child: Padding(
                padding: const EdgeInsets.only(top: kMPadding),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: kMPadding),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              const Icon(FontAwesomeIcons.locationDot),
                              Padding(
                                padding: const EdgeInsets.only(left: kMPadding),
                                child: Text('Город - N салонов', style: Theme.of(context).textTheme.titleLarge),
                              ),
                            ],
                          ),
                          const SizedBox(height: k3XLPadding),
                          Text('Города', style: Theme.of(context).textTheme.titleLarge),
                          Consumer(
                            builder: (context, ref, child) => OutlinedButton(
                              onPressed: () => ref.read(allCitiesProvider.notifier).change(),
                              child: ref.watch(allCitiesProvider) ? const Text('все города') : const Text('все салоны'),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const TabBar(
                      tabs: [
                        Tab(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(FontAwesomeIcons.locationDot),
                              Padding(
                                padding: EdgeInsets.only(left: kMPadding),
                                child: Text('На карте'),
                              ),
                            ],
                          ),
                        ),
                        Tab(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(FontAwesomeIcons.list),
                              Padding(
                                padding: EdgeInsets.only(left: kMPadding),
                                child: Text('Списком'),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Expanded(
                      child: TabBarView(
                        physics: const NeverScrollableScrollPhysics(),
                        children: [StoresMapPage(storesByCity: data), StoresByCityListPage(storesByCity: data)],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          error: (error, stackTrace) => Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text('Ошибка при получении списка магазинов'),
                ElevatedButton(onPressed: () => ref.invalidate(getAllStoresProvider), child: const Text('Обновить'))
              ],
            ),
          ),
          loading: () => const Center(child: CircularProgressIndicator()),
        );
  }
}
