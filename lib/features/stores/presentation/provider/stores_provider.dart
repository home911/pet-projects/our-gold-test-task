/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:our_gold/core/getit.dart';
import 'package:our_gold/features/stores/domain/model/store_by_city.dart';
import 'package:our_gold/features/stores/domain/repository/store_repository.dart';
import 'package:our_gold/features/stores/domain/usecase/store_usecase.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'stores_provider.g.dart';

final _storeUseCase = StoreUseCase(storeRepository: getIt<StoreRepository>());

@Riverpod(keepAlive: true)
Future<List<StoreByCity>> getAllStores(GetAllStoresRef ref) async => await _storeUseCase.getAllStores();

@riverpod
class AllCities extends _$AllCities {
  @override
  bool build() => true;

  void change() => state = !state;
}