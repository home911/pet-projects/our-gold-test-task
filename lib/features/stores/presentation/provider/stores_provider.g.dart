// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stores_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$getAllStoresHash() => r'cc03a6bd00435b5b459d45299cf75eecde21190a';

/// See also [getAllStores].
@ProviderFor(getAllStores)
final getAllStoresProvider = FutureProvider<List<StoreByCity>>.internal(
  getAllStores,
  name: r'getAllStoresProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$getAllStoresHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef GetAllStoresRef = FutureProviderRef<List<StoreByCity>>;
String _$allCitiesHash() => r'1ba542c53404600f9a0b739bac021a024d6ccd7c';

/// See also [AllCities].
@ProviderFor(AllCities)
final allCitiesProvider = AutoDisposeNotifierProvider<AllCities, bool>.internal(
  AllCities.new,
  name: r'allCitiesProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$allCitiesHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$AllCities = AutoDisposeNotifier<bool>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
