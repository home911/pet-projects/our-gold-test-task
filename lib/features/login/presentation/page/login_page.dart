/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:our_gold/core/constants/size.dart';
import 'package:our_gold/core/getit.dart';
import 'package:our_gold/core/router/router_provider.dart';
import 'package:our_gold/core/utils/message_util.dart';
import 'package:our_gold/features/login/presentation/provider/auth_provider.dart';
import 'package:our_gold/features/main/presentation/page/main_page.dart';

class LoginPage extends HookConsumerWidget {
  static const name = 'loginPage';

  const LoginPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final loginController = useTextEditingController();
    final passwordController = useTextEditingController();
    return Scaffold(
      body: SingleChildScrollView(
        physics: const ClampingScrollPhysics(),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(kXXLPadding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Align(child: Icon(Icons.person, color: Colors.black, size: 200)),
                Text('Войти', style: Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.bold)),
                const SizedBox(height: kSPadding),
                const Text('Добро пожаловать назад!\nВойдите в свой аккаунт для дальнейшей работы'),
                const SizedBox(height: k3XLPadding),
                Form(
                  child: Column(
                    children: [
                      TextFormField(
                        controller: loginController,
                        decoration: const InputDecoration(hintText: 'Логин', suffixIcon: Icon(Icons.person)),
                      ),
                      const SizedBox(height: kXLPadding),
                      TextFormField(
                        controller: passwordController,
                        decoration: const InputDecoration(hintText: 'Пароль', suffixIcon: Icon(FontAwesomeIcons.eyeSlash)),
                        obscureText: true,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: kLPadding),
                  child: Row(
                    children: [
                      Container(
                        height: 24,
                        width: 24,
                        decoration: BoxDecoration(border: Border.all(color: Colors.black), borderRadius: BorderRadius.circular(kSRadius)),
                        child: Checkbox(
                          value: true,
                          activeColor: Colors.transparent,
                          side: BorderSide.none,
                          checkColor: Colors.black,
                          onChanged: (value) {},
                          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: kLPadding),
                        child: Text('Запомнить меня'),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: k4XLPadding),
                  child: Row(
                    children: [
                      Expanded(
                        child: ElevatedButton(
                          onPressed: () async {
                            try {
                              await ref.read(authenticationProvider(loginController.text, passwordController.text).future);
                              router.pushNamed(MainPage.name);
                            } catch (e) {
                              getIt<MessageUtil>().error('Ошибка при аутентификации');
                            }
                          },
                          child: const Text('Войти'),
                        ),
                      ),
                    ],
                  ),
                ),
                const Align(
                  child: Padding(
                    padding: EdgeInsets.only(top: kSPadding),
                    child: Text('Забыли пароль?'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
