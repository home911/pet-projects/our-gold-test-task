/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:our_gold/core/getit.dart';
import 'package:our_gold/core/utils/logger.dart';
import 'package:our_gold/core/utils/storage/storage_util.dart';
import 'package:our_gold/features/login/domain/repository/auth_repository.dart';
import 'package:our_gold/features/login/domain/usecase/auth_usecase.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'auth_provider.g.dart';

final _authUseCase = AuthUseCase(authRepository: getIt<AuthRepository>(), storageUtil: getIt<StorageUtil>());

@riverpod
Future<void> authentication(AuthenticationRef ref, String login, String password) async {
  try {
    return await _authUseCase.authentication(login, password);
  } catch (e, st) {
    getIt<Log>().e('Ошибка при аутентификации', error: e, stackTrace: st);
    rethrow;
  }
}

@riverpod
Future<bool> checkToken(CheckTokenRef ref) async {
  return await _authUseCase.checkToken();
}
