// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$authenticationHash() => r'9f7a89dcf6cbb3cc44da1971e35174f2fc0f6833';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [authentication].
@ProviderFor(authentication)
const authenticationProvider = AuthenticationFamily();

/// See also [authentication].
class AuthenticationFamily extends Family<AsyncValue<void>> {
  /// See also [authentication].
  const AuthenticationFamily();

  /// See also [authentication].
  AuthenticationProvider call(
    String login,
    String password,
  ) {
    return AuthenticationProvider(
      login,
      password,
    );
  }

  @override
  AuthenticationProvider getProviderOverride(
    covariant AuthenticationProvider provider,
  ) {
    return call(
      provider.login,
      provider.password,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'authenticationProvider';
}

/// See also [authentication].
class AuthenticationProvider extends AutoDisposeFutureProvider<void> {
  /// See also [authentication].
  AuthenticationProvider(
    String login,
    String password,
  ) : this._internal(
          (ref) => authentication(
            ref as AuthenticationRef,
            login,
            password,
          ),
          from: authenticationProvider,
          name: r'authenticationProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$authenticationHash,
          dependencies: AuthenticationFamily._dependencies,
          allTransitiveDependencies:
              AuthenticationFamily._allTransitiveDependencies,
          login: login,
          password: password,
        );

  AuthenticationProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.login,
    required this.password,
  }) : super.internal();

  final String login;
  final String password;

  @override
  Override overrideWith(
    FutureOr<void> Function(AuthenticationRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: AuthenticationProvider._internal(
        (ref) => create(ref as AuthenticationRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        login: login,
        password: password,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _AuthenticationProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is AuthenticationProvider &&
        other.login == login &&
        other.password == password;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, login.hashCode);
    hash = _SystemHash.combine(hash, password.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin AuthenticationRef on AutoDisposeFutureProviderRef<void> {
  /// The parameter `login` of this provider.
  String get login;

  /// The parameter `password` of this provider.
  String get password;
}

class _AuthenticationProviderElement
    extends AutoDisposeFutureProviderElement<void> with AuthenticationRef {
  _AuthenticationProviderElement(super.provider);

  @override
  String get login => (origin as AuthenticationProvider).login;
  @override
  String get password => (origin as AuthenticationProvider).password;
}

String _$checkTokenHash() => r'41f47a17b9dcd3f899a6b7b6e39e325da9050d85';

/// See also [checkToken].
@ProviderFor(checkToken)
final checkTokenProvider = AutoDisposeFutureProvider<bool>.internal(
  checkToken,
  name: r'checkTokenProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$checkTokenHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef CheckTokenRef = AutoDisposeFutureProviderRef<bool>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
