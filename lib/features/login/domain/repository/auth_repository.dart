/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

abstract interface class AuthRepository {

  Future<String> authentication(String login, String password);

}