/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:our_gold/core/getit.dart';
import 'package:our_gold/core/utils/logger.dart';
import 'package:our_gold/core/utils/storage/storage_util.dart';
import 'package:our_gold/features/login/domain/repository/auth_repository.dart';

class AuthUseCase {
  final AuthRepository _authRepository;
  final StorageUtil _storageUtil;

  const AuthUseCase({
    required AuthRepository authRepository,
    required StorageUtil storageUtil,
  })
      : _authRepository = authRepository,
        _storageUtil = storageUtil;

  Future<void> authentication(String login, String password) async {
    try {
      final token = await _authRepository.authentication(login, password);
      await _storageUtil.writeTokenInfo(token);
    } catch (e, st) {
      getIt<Log>().e('Ошибка при аутентификации', error: e, stackTrace: st);
      rethrow;
    }
  }

  Future<bool> checkToken() async {
    return (await _storageUtil.getAuthToken()) != null;
  }

}