/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:dio/dio.dart';
import 'package:our_gold/core/getit.dart';
import 'package:our_gold/core/utils/api_util.dart';
import 'package:our_gold/features/login/domain/repository/auth_repository.dart';

class AuthRepositoryImpl implements AuthRepository {
  final dio = getIt<Dio>();

  @override
  Future<String> authentication(String login, String password) async {
    final result = await dio.postUri(ApiUtil.loginUri, data: {
      'login': login,
      'password': password,
    });
    return result.data['token'];
  }
}
