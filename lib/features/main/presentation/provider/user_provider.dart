/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:our_gold/core/getit.dart';
import 'package:our_gold/core/utils/storage/storage_util.dart';
import 'package:our_gold/features/main/domain/model/user_model.dart';
import 'package:our_gold/features/main/domain/repository/user_repository.dart';
import 'package:our_gold/features/main/domain/usecase/user_usecase.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'user_provider.g.dart';

final _userUseCase = UserUseCase(userRepository: getIt<UserRepository>(), storageUtil: getIt<StorageUtil>());

@riverpod
Future<UserModel> getUser(GetUserRef ref) async {
  return await _userUseCase.getUser();
}
