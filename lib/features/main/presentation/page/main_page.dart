/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:our_gold/core/constants/size.dart';
import 'package:our_gold/core/getit.dart';
import 'package:our_gold/core/router/router_provider.dart';
import 'package:our_gold/core/utils/message_util.dart';
import 'package:our_gold/core/utils/storage/storage_util.dart';
import 'package:our_gold/features/login/presentation/page/login_page.dart';
import 'package:our_gold/features/main/domain/repository/user_repository.dart';
import 'package:our_gold/features/main/domain/usecase/user_usecase.dart';
import 'package:our_gold/features/main/presentation/provider/user_provider.dart';

class MainPage extends ConsumerWidget {
  static const name = 'mainPage';

  const MainPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SafeArea(
      child: ref.watch(getUserProvider).when(
            data: (user) => Stack(
              children: [
                Container(
                    height: 250,
                    color: Theme.of(context).primaryColor,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        SizedBox(
                          width: 250,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                user.name,
                                style: Theme.of(context).textTheme.titleLarge?.copyWith(
                                      fontWeight: FontWeight.bold,
                                      color: Theme.of(context).colorScheme.onPrimary,
                                    ),
                              ),
                              Text(
                                user.post,
                                style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                                      fontStyle: FontStyle.italic,
                                      fontWeight: FontWeight.w800,
                                      color: Theme.of(context).colorScheme.onPrimary,
                                    ),
                              ),
                              Text(
                                user.division,
                                style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                                      fontStyle: FontStyle.italic,
                                      fontWeight: FontWeight.w100,
                                      color: Theme.of(context).colorScheme.onPrimary,
                                    ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    )),
                const Positioned(
                  top: 140,
                  child: Row(
                    children: [
                      Icon(Icons.person, size: 150, color: Colors.black),
                    ],
                  ),
                ),
                Positioned(
                  top: kMPadding,
                  right: kMPadding,
                  child: Container(
                    decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(kSRadius)),
                    child: Padding(
                      padding: const EdgeInsets.all(kXSPadding),
                      child: GestureDetector(
                        onTap: () async {
                          try {
                            await UserUseCase(userRepository: getIt<UserRepository>(), storageUtil: getIt<StorageUtil>()).logout();
                            router.goNamed(LoginPage.name);
                          } catch (e) {
                            getIt<MessageUtil>().error('Произошла ошибка при выходе из аккаунта');
                          }
                        },
                        child: const Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text('Выйти'),
                            SizedBox(width: kMPadding),
                            Icon(Icons.exit_to_app),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
            error: (error, stackTrace) => Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text('Ошибка при загрузке пользователя'),
                  ElevatedButton(onPressed: () => ref.invalidate(getUserProvider), child: const Text('Обновить'))
                ],
              ),
            ),
            loading: () => const Center(child: CircularProgressIndicator()),
          ),
    );
  }
}
