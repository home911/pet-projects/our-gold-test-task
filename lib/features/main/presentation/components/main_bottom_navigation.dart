/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'package:our_gold/features/main/presentation/components/main_tab_item.dart';

class MainBottomNavigation extends StatelessWidget {
  const MainBottomNavigation({
    super.key,
    required this.currentIndex,
    required this.onSelectTab,
  });

  final int currentIndex;
  final Function(int, {bool isInitial}) onSelectTab;

  @override
  Widget build(BuildContext context) {
    return NavigationBar(
      selectedIndex: currentIndex,
      onDestinationSelected: (value) => onSelectTab(value, isInitial: value == currentIndex),
      destinations: MainTabItem.values.map((tab) => NavigationDestination(icon: Icon(tab.icon), label: tab.name)).toList(),
    );
  }
}
