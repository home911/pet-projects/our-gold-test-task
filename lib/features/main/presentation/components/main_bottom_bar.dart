/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:our_gold/core/getit.dart';
import 'package:our_gold/core/router/router_provider.dart';
import 'package:our_gold/core/utils/message_util.dart';
import 'package:our_gold/features/main/presentation/page/main_page.dart';
import 'package:our_gold/main.dart';

import 'main_bottom_navigation.dart';
import 'main_tab_item.dart';

final _countExitAttemptsProvider = StateProvider((ref) => 0);

class MainBottomBar extends ConsumerWidget {
  static const routePath = '/catalogBottomBar';
  static const routeName = 'catalog_bottom_bar';
  final StatefulNavigationShell navigationShell;

  const MainBottomBar({super.key, required this.navigationShell});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return WillPopScope(
      onWillPop: _checkCurrentTab,
      child: Scaffold(
        body: navigationShell,
        bottomNavigationBar: MainBottomNavigation(currentIndex: navigationShell.currentIndex, onSelectTab: _selectTab),
      ),
    );
  }

  Future<bool> _checkCurrentTab() async {
    final isFirstRouteInCurrentTab = !await navigationShell.shellRouteContext.navigatorKey.currentState!.maybePop();
    if (isFirstRouteInCurrentTab && navigationShell.currentIndex != MainTabItem.home.index) {
      container.read(_countExitAttemptsProvider.notifier).update((state) => 0);
      _selectTab(MainTabItem.home.index);
      return false;
    } else if (isFirstRouteInCurrentTab && navigationShell.currentIndex == MainTabItem.home.index) {
      container.read(_countExitAttemptsProvider.notifier).update((state) => state + 1);
      if (container.read(_countExitAttemptsProvider) > 1) {
        router.goNamed(MainPage.name);
        return false;
      } else {
        getIt<MessageUtil>().info('Для выхода из приложения ещё раз нажмите кнопку "Назад"');
        return false;
      }
    }
    return isFirstRouteInCurrentTab;
  }

  void _selectTab(int index, {bool isInitial = false}) => navigationShell.goBranch(index, initialLocation: isInitial);
}
