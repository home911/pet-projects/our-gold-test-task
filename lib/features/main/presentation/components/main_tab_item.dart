/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

enum MainTabItem {
  home('Главная', FontAwesomeIcons.house),
  scanner('Сканер товаров', FontAwesomeIcons.qrcode),
  shops('Карта магазинов', FontAwesomeIcons.map);

  final String name;
  final IconData icon;

  const MainTabItem(this.name, this.icon);
}
