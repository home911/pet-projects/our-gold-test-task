/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

class UserModel {
  final String name;
  final String division;
  final String post;

  const UserModel({
    required this.name,
    required this.division,
    required this.post,
  });

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'division': division,
      'post': post,
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel(
      name: map['name'] as String,
      division: map['division'] as String,
      post: map['post'] as String,
    );
  }

}