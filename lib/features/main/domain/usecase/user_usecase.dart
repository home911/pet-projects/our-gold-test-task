/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:our_gold/core/getit.dart';
import 'package:our_gold/core/utils/logger.dart';
import 'package:our_gold/core/utils/storage/storage_util.dart';
import 'package:our_gold/features/main/domain/model/user_model.dart';
import 'package:our_gold/features/main/domain/repository/user_repository.dart';

class UserUseCase {
  final UserRepository _userRepository;
  final StorageUtil _storageUtil;

  const UserUseCase({
    required UserRepository userRepository,
    required StorageUtil storageUtil,
  })  : _userRepository = userRepository,
        _storageUtil = storageUtil;

  Future<UserModel> getUser() async {
    try {
      final user = await _storageUtil.getUser();
      if (user == null) {
            final newUser = await _userRepository.getUser();
            await _storageUtil.saveUser(newUser);
            return newUser;
          }
      return user;
    } catch (e, st) {
      getIt<Log>().e('Ошибка при получении пользователя', error: e, stackTrace: st);
      rethrow;
    }
  }

  Future<void> logout() async {
    try {
      return await _storageUtil.clearSession();
    } catch (e, st) {
      getIt<Log>().e('Ошибка при выходе из профиля', error: e, stackTrace: st);
      rethrow;
    }
  }

}