/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:our_gold/features/main/domain/model/user_model.dart';

abstract interface class UserRepository {

  Future<UserModel> getUser();

}