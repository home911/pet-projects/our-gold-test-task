/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'dart:io';

import 'package:dio/dio.dart';
import 'package:our_gold/core/getit.dart';
import 'package:our_gold/core/utils/api_util.dart';
import 'package:our_gold/core/utils/storage/storage_util.dart';
import 'package:our_gold/features/main/domain/model/user_model.dart';
import 'package:our_gold/features/main/domain/repository/user_repository.dart';

class UserRepositoryImpl implements UserRepository {
  final dio = getIt<Dio>();
  final _storage = getIt<StorageUtil>();

  @override
  Future<UserModel> getUser() async {
    final result = await dio.getUri(
      ApiUtil.userUri,
      options: Options(headers: {HttpHeaders.authorizationHeader: await _storage.getAuthToken()})
    );
    return UserModel.fromMap(result.data);
  }

}
