/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:our_gold/features/scanner/domain/model/product_model.dart';

abstract interface class ProductRepository {

  Future<ProductModel> getProduct(String barcode);

}