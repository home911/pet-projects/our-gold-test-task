/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:our_gold/features/scanner/domain/model/product_model.dart';
import 'package:our_gold/features/scanner/domain/repository/product_repository.dart';

class ProductUseCase {
  final ProductRepository _productRepository;

  const ProductUseCase({
    required ProductRepository productRepository,
  }) : _productRepository = productRepository;

  Future<ProductModel> getProduct(String barcode) async {
    return await _productRepository.getProduct(barcode);
  }

}