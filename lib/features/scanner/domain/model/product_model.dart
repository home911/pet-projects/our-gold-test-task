/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

class ProductModel {
  final String code;
  final String name;
  final List<PriceWithSize> prices;
  final String brand;
  final List<ProductProperty> properties;
  final List<ProductAttachment> attachments;

  const ProductModel({
    required this.code,
    required this.name,
    required this.prices,
    required this.brand,
    required this.properties,
    required this.attachments,
  });

  factory ProductModel.fromMap(Map<String, dynamic> map) {
    return ProductModel(
      code: map['code'] as String,
      name: map['name'] as String,
      prices: (map['prices'] as List).map((e) => PriceWithSize.fromMap(e)).toList(),
      brand: map['brand'] as String,
      properties: (map['properties'] as List).map((e) => ProductProperty.fromMap(e)).toList(),
      attachments: (map['attachments'] as List).map((e) => ProductAttachment.fromMap(e)).toList(),
    );
  }
}

class PriceWithSize {
  final double price;
  final String size;

  const PriceWithSize({
    required this.price,
    required this.size,
  });

  factory PriceWithSize.fromMap(Map<String, dynamic> map) {
    return PriceWithSize(
      price: num.parse(map['price'].toString()).toDouble(),
      size: map['size'] as String,
    );
  }
}

class ProductProperty {
  final String name;
  final String value;

  const ProductProperty({
    required this.name,
    required this.value,
  });

  factory ProductProperty.fromMap(Map<String, dynamic> map) {
    return ProductProperty(
      name: map['name'] as String,
      value: map['value'] as String,
    );
  }
}

class ProductAttachment {
  final String name;
  final String path;

  const ProductAttachment({
    required this.name,
    required this.path,
  });

  factory ProductAttachment.fromMap(Map<String, dynamic> map) {
    return ProductAttachment(
      name: map['name'] as String,
      path: map['path'] as String,
    );
  }
}
