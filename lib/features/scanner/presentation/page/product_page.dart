/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:our_gold/core/constants/size.dart';
import 'package:our_gold/core/getit.dart';
import 'package:our_gold/core/utils/format_util.dart';
import 'package:our_gold/features/scanner/domain/model/product_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_player/video_player.dart';

class ProductPage extends StatelessWidget {
  final ProductModel product;

  const ProductPage({super.key, required this.product});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(kLPadding),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ProductImages(attachments: product.attachments),
              Text(product.name, style: Theme.of(context).textTheme.titleLarge),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: kMPadding),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(product.brand),
                    Text(
                      'Код: ${product.code}',
                      style: Theme.of(context).textTheme.bodyMedium?.copyWith(color: Theme.of(context).disabledColor),
                    ),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(kMPadding),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(kSRadius),
                  color: Theme.of(context).disabledColor.withOpacity(0.1),
                ),
                child: Row(
                  children: [
                    Text(
                      FormatUtil.price(product.prices.first.price),
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(width: kMPadding),
                    Text(
                      FormatUtil.price(product.prices.first.price),
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(
                            fontWeight: FontWeight.bold,
                            color: Theme.of(context).disabledColor,
                            decoration: TextDecoration.lineThrough,
                          ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: kLPadding),
                child: Text('Описание и характеристики', style: Theme.of(context).textTheme.titleLarge),
              ),
              Text('Код товара: ${product.code}'),
              const Padding(
                padding: EdgeInsets.symmetric(vertical: kLPadding),
                child: Text('Описание'),
              ),
              Flexible(
                child: ListView.builder(
                  physics: const ClampingScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: product.properties.length,
                  itemBuilder: (context, index) {
                    final property = product.properties[index];
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(property.name),
                        Expanded(child: Text('.' * 100, maxLines: 1)),
                        property.value.length < 20
                            ? Text(
                                property.value,
                                style: Theme.of(context).textTheme.bodyMedium?.copyWith(fontWeight: FontWeight.bold),
                              )
                            : Container(
                                constraints: const BoxConstraints(maxWidth: 80),
                                child: Text(
                                  property.value,
                                  style: Theme.of(context).textTheme.bodyMedium?.copyWith(fontWeight: FontWeight.bold),
                                ),
                              ),
                      ],
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ProductImages extends StatefulWidget {
  const ProductImages({
    super.key,
    required this.attachments,
  });

  final List<ProductAttachment> attachments;

  @override
  State<ProductImages> createState() => _ProductImagesState();
}

class _ProductImagesState extends State<ProductImages> with TickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: widget.attachments.length, vsync: this, animationDuration: const Duration(seconds: 0));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox.square(
      dimension: MediaQuery.of(context).size.width,
      child: Stack(
        children: [
          TabBarView(
            controller: _tabController,
            children: List.generate(widget.attachments.length, (index) {
              return ProductImage(imagePath: widget.attachments[index].path);
            }),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: kSPadding),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: TabPageSelector(controller: _tabController),
            ),
          )
        ],
      ),
    );
  }

}

class ProductImage extends StatefulWidget {
  const ProductImage({
    super.key,
    required this.imagePath,
  });

  final String imagePath;

  @override
  State<ProductImage> createState() => _ProductImageState();
}

class _ProductImageState extends State<ProductImage> {
  VideoPlayerController? _controller;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      if (widget.imagePath.endsWith('.mp4')) {
        final name = widget.imagePath.split('/').last;
        final pathFile = '${(await getTemporaryDirectory()).path}/$name';
        if (!(await File(pathFile).exists())) {
          await File(pathFile).create();
          await getIt<Dio>().downloadUri(Uri.parse(widget.imagePath), pathFile);
        }
        _controller = VideoPlayerController.file(File(pathFile))
          ..initialize().then((_) {
            setState(() {});
          })..play()..setLooping(true);
      }
    });

  }

  @override
  Widget build(BuildContext context) {
    if (widget.imagePath.endsWith('.mp4')) {
      if (_controller == null) {
        return const Center(child: CircularProgressIndicator());
      }
      return VideoPlayer(_controller!);
    }
    return CachedNetworkImage(
      imageUrl: widget.imagePath,
      fit: BoxFit.cover,
      placeholder: (context, url) => const Center(
        child: SizedBox(width: 40.0, height: 40.0, child: CircularProgressIndicator()),
      ),
      errorWidget: (context, url, error) => Icon(Icons.error, color: Theme.of(context).colorScheme.error),
    );
  }
}