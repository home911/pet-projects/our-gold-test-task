/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import 'package:our_gold/core/constants/size.dart';
import 'package:our_gold/core/getit.dart';
import 'package:our_gold/core/utils/logger.dart';
import 'package:our_gold/core/utils/message_util.dart';
import 'package:our_gold/features/scanner/presentation/page/product_page.dart';
import 'package:our_gold/features/scanner/presentation/provider/product_provider.dart';

final _isLoadingProduct = StateProvider<bool>((ref) => false);

class ScannerPage extends ConsumerStatefulWidget {
  static const name = 'scannerPage';

  const ScannerPage({super.key});

  @override
  ConsumerState<ScannerPage> createState() => _ScannerPageState();
}

class _ScannerPageState extends ConsumerState<ScannerPage> {
  Timer? _debounce;
  late MobileScannerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = MobileScannerController(
      detectionSpeed: DetectionSpeed.normal,
      facing: CameraFacing.back,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final double width = size.width - 50;
    final double height = size.height;
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: k3XLPadding, vertical: k4XLPadding),
        child: Column(
          children: [
            Text(
              'Наведите камеру на штрих-код товара, чтобы вывести подробную информацию о нём',
              style: Theme.of(context).textTheme.titleLarge,
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: kLPadding),
            Expanded(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(kLRadius),
                child: MobileScanner(
                  scanWindow: Rect.fromCenter(center: Offset(size.width / 2, size.height / 2), width: width, height: height),
                  controller: _controller,
                  overlay: Consumer(
                    builder: (context, ref, child) {
                      final isLoading = ref.watch(_isLoadingProduct);
                      return isLoading ? const Center(child: CircularProgressIndicator()) : const SizedBox();
                    },
                  ),
                  onDetect: (capture) async {
                    final List<Barcode> barcodes = capture.barcodes;
                    for (final barcode in barcodes) {
                      if (barcode.rawValue != null && barcode.rawValue!.isNotEmpty && !ref.read(_isLoadingProduct)) {
                        try {
                          getIt<Log>().d('Штрихкод: ${barcode.rawValue}');
                          ref.read(_isLoadingProduct.notifier).update((state) => true);
                          if (_debounce?.isActive ?? false) _debounce?.cancel();
                          _debounce = Timer(const Duration(seconds: 1), () async {
                            final product = await ref.read(getProductProvider(barcode.rawValue!).future);
                            await _controller.stop();
                            if (context.mounted) {
                              await Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => ProductPage(product: product)),
                              );
                              await _controller.start();
                              ref.read(_isLoadingProduct.notifier).update((state) => false);
                            }
                          });
                        } catch (e, st) {
                          getIt<Log>().e('Не удалось получить товар по штрихкоду', error: e, stackTrace: st);
                          ref.read(_isLoadingProduct.notifier).update((state) => false);
                          getIt<MessageUtil>().error('Не удалось получить товар по штрихкоду');
                        }
                      }
                    }
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
