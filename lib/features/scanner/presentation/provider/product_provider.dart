/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:our_gold/core/getit.dart';
import 'package:our_gold/features/scanner/domain/model/product_model.dart';
import 'package:our_gold/features/scanner/domain/repository/product_repository.dart';
import 'package:our_gold/features/scanner/domain/usecase/product_usecase.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'product_provider.g.dart';

final _productUseCase = ProductUseCase(productRepository: getIt<ProductRepository>());

@riverpod
Future<ProductModel> getProduct(GetProductRef ref, String barcode) async {
  return await _productUseCase.getProduct(barcode);
}
