/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'dart:io';

import 'package:dio/dio.dart';
import 'package:our_gold/core/getit.dart';
import 'package:our_gold/core/utils/api_util.dart';
import 'package:our_gold/core/utils/storage/storage_util.dart';
import 'package:our_gold/features/scanner/domain/model/product_model.dart';
import 'package:our_gold/features/scanner/domain/repository/product_repository.dart';

class ProductRepositoryImpl implements ProductRepository {
  final dio = getIt<Dio>();
  final _storage = getIt<StorageUtil>();

  @override
  Future<ProductModel> getProduct(String barcode) async {
    final result = await dio.getUri(
        ApiUtil.productUri(barcode),
        options: Options(headers: {HttpHeaders.authorizationHeader: await _storage.getAuthToken()})
    );
    return ProductModel.fromMap(result.data);
  }

}